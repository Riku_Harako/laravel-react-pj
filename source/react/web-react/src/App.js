import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react'
import axios from "axios";

export default function App() {

  const [books, setBooks] = useState([]);

  useEffect(()=> {
    const get = async () => {
      const res = await axios("http://localhost:8080/api/books");
      const arr =JSON.parse(JSON.stringify(res.data));
      setBooks(arr)
    }
    get();
  },[])

  return (
    <div className="App">
      <h1>「reactからlaravelのapiを叩く」の回</h1>
      <h2>下記、取得データ</h2>
      {books.map((book) => (
        <div>
          <p>{book.Email}</p>
        </div>
      ))}
      {/* <p>{books.title}</p>
      <p>{books.mainCharacter}</p>
      <p>{books.kinds}</p>
      <p>{books.boss}</p> */}
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          react導入  成功！
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
    </div>
  );
}
