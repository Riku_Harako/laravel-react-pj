import React, { useState, useEffect } from 'react'
import ReactPaginate from 'react-paginate';
import axios from "axios";

export default function TextList(){

    const [books, setBooks] = useState([]);
    const [start, setStart] = useState(0);
    //1ページ10件
    const perPage = 10;

    const pageChange = (data)  => {
        let pageNumber = data['selected'];
        setStart(pageNumber * perPage);
    }

    useEffect(()=> {
      const get = async () => {
        const res = await axios("http://localhost:8080/api/books");
        const arr =JSON.parse(JSON.stringify(res.data));
        setBooks(arr)
      }
      get();
    },[])

    return (
        <div id='list'>
            <table border="1">
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>Title</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                {books.slice(start, start + perPage).map((book) => (
                    <tr key={book.title} >
                        <td><input type="checkbox"/></td>
                        <td>{book.title}</td>
                        <td>{book.userName}</td>
                        <td>{book.email}</td>
                        <td>{book.status}</td>
                        <td>~~~</td>
                    </tr>
                ))}
                </tbody>
            </table>

            <ReactPaginate
                pageCount={Math.ceil(books.length / perPage)}
                marginPagesDisplayed={2} //先頭と末尾に表示するページの数。今回は2としたので1,2…今いるページの前後…後ろから2番目, 1番目 のように表示されます。
                pageRangeDisplayed={5} //上記の「今いるページの前後」の番号をいくつ表示させるかを決めます。
                onPageChange={pageChange} //ページネーションのリンクをクリックしたときのイベント(詳しくは下で解説します)
                containerClassName="pagination justify-center" // ul(pagination本体)
                pageClassName="page-item" // li
                pageLinkClassName="page-link rounded-full" // a
                activeClassName="active" // active.li
                activeLinkClassName="active" // active.li < a

                // 戻る・進む関連
                previousClassName="page-item" // li
                nextClassName="page-item" // li
                previousLabel={'<'} // a
                previousLinkClassName="previous-link"
                nextLabel={'>'} // a
                nextLinkClassName="next-link"

                // 先頭 or 末尾に行ったときにそれ以上戻れ(進め)なくする
                disabledClassName="disabled-button d-none"

                // 中間ページの省略表記関連
                breakLabel="..."
                breakClassName="page-item"
                breakLinkClassName="page-link"
            />
        </div>
    )
}