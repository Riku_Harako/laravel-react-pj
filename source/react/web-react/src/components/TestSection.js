import TestHeader from './TestHeader';
import TestSideBar from './TestSideBar';
import TestList from './TestList';
import '../Test.css'

export default function TestSection() {
    return (
        <div id='section'>
            <TestHeader />
            <div className="blank"/>
            <div className="flex-box">
                <TestSideBar />
                <TestList />
            </div>
        </div>
    )
}